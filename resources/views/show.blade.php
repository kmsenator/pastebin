@extends('layout')

@section('table')
    @include('sidebar_toggle')

    <div class="sidebar">

        <div class="options">
            <ul>
                <li><a href="{{ route('home') }}" class="button new">New</a></li>
                <li><a href="{{ route('edit', $paste->hash) }}" class="button fork">Fork</a></li>
                <li><a target="_blank" href="{{ route('raw', $paste->hash) }}" class="button raw">Raw</a></li>
            </ul>
        </div>

    </div>

    <div class="show-container">
        <pre class="prettyprint linenums selectable">
{{ $paste->code }}
        </pre>
    </div>
@stop
