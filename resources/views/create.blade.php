@extends('layout')

            
@section('table')
    @include('sidebar_toggle')

    <form action="{{ url('/') }}" method="POST" class="editor-form">
        {!! csrf_field() !!}

        <div class="sidebar">
   

            <div class="options">
                {!! $errors->first('code', '<p>:message</p><br>') !!}
                <ul>
                    <li><input type="submit" value="Save" class="button"></li>
					<li><select name="public"><option value="1" selected>public</option><option value="0">private</option></select>
                    <li><input type="reset" value="Clear" class="button"></li>
                </ul>
            </div>

            <div>
                    <p>
                        Last 10 pastes</p>

									@foreach ($pastes as $paste)
                                        <div><a href="/fork/{{ $paste->hash }}">{{ $paste->id }}. {{ $paste->created_at }}</a></div>
									@endforeach
									
                    </p>
                </div>
        </div>

            
                
        <div class="editor-container">
			<textarea name="code" class="editor mousetrap" wrap="off"></textarea>
		</div>
    </form>
@stop
