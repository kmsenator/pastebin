@extends('layout')

@section('table')
    @include('sidebar_toggle')

    <form action="{{ route('edit', $paste) }}" method="POST" class="editor-form">
        {!! csrf_field() !!}

        <div class="sidebar create">

            <div class="options">
                {!! $errors->first('code', '<p>:message</p><br>') !!}
                <ul>
                    <li><input type="submit" value="Save" class="button"></li>
					<li><select name="public"><option value="1" selected>public</option><option value="0">private</option></select>
                    <li><input type="reset" value="Clear" class="button"></li>
                    <li>
                        <a href="{{ route('show', $paste->hash) }}" class="button back">
                            <i class="fa fa-arrow-circle-o-left"></i> Back
                        </a>
                    </li>
                </ul>
            </div>

        </div>

        @include('editor')
    </form>
@stop
