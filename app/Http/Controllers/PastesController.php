<?php

namespace App\Http\Controllers;

use App\Paste;
use App\Http\Requests\PasteRequest;

class PastesController extends Controller
{
    public function create()
    {
		$pastes = Paste::last_public_10();
        return view('create', compact('pastes'));
    }

    public function post(PasteRequest $request)
    {
        $paste = Paste::fromRequest($request);

        return redirect()->route('show', $paste->hash);
    }

    public function show(Paste $paste)
    {
        return view('show', compact('paste'));
    }

    public function raw(Paste $paste)
    {
        return view('raw', compact('paste'));
    }

    public function edit(Paste $paste)
    {
        return view('edit', compact('paste'));
    }

    public function fork(PasteRequest $request, Paste $paste)
    {
        $paste = Paste::fromFork($paste, $request);

        return redirect()->route('show', $paste->hash);
    }
	
}


